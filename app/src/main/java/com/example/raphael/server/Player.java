package com.example.raphael.server;

import android.media.MediaPlayer;
import android.os.Environment;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Tech Fred on 2016-06-12.
 */
public class Player {
    private static Player INSTANCE;
    private ArrayList<FichierMusique> listemusique;
    private int currentID;
    private MediaPlayer mediaPlayer;
    private boolean isPaused;
    private boolean isLooping;

    private Player() {

        listemusique = DummyBuilder.DummyList();
        currentID = 0;
        mediaPlayer = new MediaPlayer();

    }

    public static Player getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Player();
        }
        return INSTANCE;
    }

    public FichierMusique play(boolean stream) {
        if (!stream) {
            try {
                if (isPaused) {
                    mediaPlayer.start();
                    isPaused = false;
                } else {
                    mediaPlayer.reset();
                    mediaPlayer.setDataSource(Environment.getExternalStorageDirectory() + listemusique.get(currentID).getLocalPath());
                    mediaPlayer.prepare();
                    mediaPlayer.start();
                    isPaused = false;
                    isLooping = false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        return listemusique.get(currentID);
    }

    public FichierMusique stop(boolean stream) {
        if (!stream) {
            mediaPlayer.stop();

        }


        return listemusique.get(currentID);
    }

    public FichierMusique next(boolean stream) {
        if (currentID < listemusique.size() - 1) {
            currentID = currentID + 1;
        }
        if (!stream) {
            stop(stream);
            play(stream);
            isPaused = false;
        }

        return listemusique.get(currentID);

    }

    public FichierMusique previous(boolean stream) {
        if (currentID > 0) {
            currentID = currentID - 1;
        }
        if (!stream) {
            stop(stream);
            play(stream);

        }

        return listemusique.get(currentID);
    }

    public FichierMusique pause(boolean stream) {
        if (!stream) {
            mediaPlayer.pause();
            isPaused = true;
        }

        return listemusique.get(currentID);
    }

    public FichierMusique random(boolean stream) {
        Random r = new Random();
        currentID = r.nextInt(listemusique.size() - 1);


        if (!stream) {
            stop(stream);
            play(stream);
        }

        return listemusique.get(currentID);
    }

    public FichierMusique loop(boolean stream) {
        Random r = new Random();
        if (!stream) {
            if (isLooping) {
                mediaPlayer.setLooping(false);
                isLooping = false;
            } else {
                mediaPlayer.setLooping(true);
                isLooping = true;
            }
        }
        return listemusique.get(currentID);
    }


}
