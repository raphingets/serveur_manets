package com.example.raphael.server;

import java.util.ArrayList;

/**
 * Created by Tech Fred on 2016-06-12.
 */
public class DummyBuilder {

    public static ArrayList DummyList (){
        ArrayList<FichierMusique> listeFichiersMusique = new ArrayList();

        listeFichiersMusique.add(new FichierMusique("Superlove","Avicii","Avicii","Electro",0,"/Androidtest/fichier1.mp3","Superlove", "/Androidtest/fichier1.mp3",322000));
        listeFichiersMusique.add(new FichierMusique("Duo du balcon","Accrophone","Accrophone 2","Hip-Hop",1,"/Androidtest/fichier2.mp3","Duo du balcon", "/Androidtest/fichier2.mp3",227000));
        listeFichiersMusique.add(new FichierMusique("Duo du balcon","Accrophone","Accrophone 3","Hip-Hop",2,"/Androidtest/fichier3.mp3","La realite frappe fort", "/Androidtest/fichier3.mp3",306000));
        listeFichiersMusique.add(new FichierMusique("Duo du balcon","Accrophone","Accrophone 4","Hip-Hop",3,"/Androidtest/fichier4.mp3","coin de paradis", "/Androidtest/fichier4.mp3",197000));
        listeFichiersMusique.add(new FichierMusique("Could be the one","Avicii","Avicii","Electro",4,"/Androidtest/fichier5.mp3","Could be the one", "/Androidtest/fichier5.mp3",208000));
        listeFichiersMusique.add(new FichierMusique("Silhouettes","Avicii","Avicii","Electro",5,"/Androidtest/fichier6.mp3","Silhouettes", "/Androidtest/fichier6.mp3",420000));
        listeFichiersMusique.add(new FichierMusique("Duo du balcon","Accrophone","Accrophone","Hip-Hop",6,"/Androidtest/fichier7.mp3","Mal chausse", "/Androidtest/fichier7.mp3",240000));
        listeFichiersMusique.add(new FichierMusique("Duo du balcon","Accrophone","Accrophone","Hip-Hop",7,"/Androidtest/fichier8.mp3","Anyway", "/Androidtest/fichier8.mp3",195000));
        listeFichiersMusique.add(new FichierMusique("Fade into darkness","Avicii","Avicii","Electro",8,"/Androidtest/fichier9.mp3","Fade into darkness", "/Androidtest/fichier9.mp3",198000));

        return listeFichiersMusique;
    }
}
