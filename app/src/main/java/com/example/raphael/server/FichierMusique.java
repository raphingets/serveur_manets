package com.example.raphael.server;

/**
 * Created by Tech Fred on 2016-06-07.
 */
public class FichierMusique {

    String album;
    String artist;
    String composer;
    String genre;
    int id;
    String localPath;
    String title;
    String serverPath;
    int duration; // en ms

    public int getDuration() {
        return duration;
    }

    public String getComposer() {
        return composer;
    }

    public String getGenre() {
        return genre;
    }

    public int getId() {
        return id;
    }

    public String getLocalPath() {
        return localPath;
    }

    public String getTitle() {
        return title;
    }

    public String getServerPath() {
        return serverPath;
    }

    public FichierMusique(String album, String artist, String composer, String genre, int id, String localPath, String title, String serverPath, int duration) {
        this.album = album;
        this.artist = artist;
        this.composer = composer;
        this.genre = genre;
        this.id = id;
        this.localPath = localPath;
        this.title = title;
        this.serverPath = serverPath;

        this.duration = duration;
    }

    public String getAlbum() {
        return album;
    }

    public String getArtist() {
        return artist;
    }
}
