package com.example.raphael.server;

import android.os.Environment;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.IOException;

import fi.iki.elonen.NanoHTTPD;

/**
 * Created by Raphael on 2016-05-31.
 */
public class Nano extends NanoHTTPD {


    public Nano(int port) throws IOException {
        super(port);


    }

    @Override
    public Response serve(IHTTPSession session) {
        Response r = new Response("Blank");
        if (session.getUri().equals("/action/play")) {
            FichierMusique fm = Player.getInstance().play(false);
            r = new Response(jsonBuilder(fm));

        } else if (session.getUri().equals("/action/stop")) {
            FichierMusique fm = Player.getInstance().stop(false);
            r = new Response(jsonBuilder(fm));

        } else if (session.getUri().equals("/action/pause")) {
            FichierMusique fm = Player.getInstance().pause(false);
            r = new Response(jsonBuilder(fm));

        } else if (session.getUri().equals("/action/next")) {

            FichierMusique fm = Player.getInstance().next(false);
            r = new Response(jsonBuilder(fm));
        } else if (session.getUri().equals("/action/previous")) {
            FichierMusique fm = Player.getInstance().previous(false);
            r = new Response(jsonBuilder(fm));

        } else if (session.getUri().equals("/action/loop")) {
            FichierMusique fm = Player.getInstance().loop(false);
            r = new Response(jsonBuilder(fm));

        } else if (session.getUri().equals("/action/random")) {
            FichierMusique fm = Player.getInstance().random(false);
            r = new Response(jsonBuilder(fm));

        } else if (session.getUri().equals("/action/shuffle")) {
            FichierMusique fm = Player.getInstance().random(false);
            r = new Response(jsonBuilder(fm));

        } else if (session.getUri().equals("/stream/play")) {
            FichierMusique fm = Player.getInstance().play(true);
            r = new Response(jsonBuilder(fm));

        } else if (session.getUri().equals("/stream/stop")) {
            FichierMusique fm = Player.getInstance().stop(true);
            r = new Response(jsonBuilder(fm));

        } else if (session.getUri().equals("/stream/pause")) {
            FichierMusique fm = Player.getInstance().pause(true);
            r = new Response(jsonBuilder(fm));

        } else if (session.getUri().equals("/stream/next")) {
            FichierMusique fm = Player.getInstance().next(true);
            r = new Response(jsonBuilder(fm));

        } else if (session.getUri().equals("/stream/previous")) {
            FichierMusique fm = Player.getInstance().previous(true);
            r = new Response(jsonBuilder(fm));

        } else if (session.getUri().equals("/stream/loop")) {
            FichierMusique fm = Player.getInstance().loop(true);
            r = new Response(jsonBuilder(fm));

        } else if (session.getUri().equals("/stream/random")) {
            FichierMusique fm = Player.getInstance().random(true);
            r = new Response(jsonBuilder(fm));
            
        } else if (session.getUri().equals("/stream/shuffle")) {
            FichierMusique fm = Player.getInstance().random(true);
            r = new Response(jsonBuilder(fm));

        } else if (session.getUri().matches("/Androidtest(.*)")) {
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(Environment.getExternalStorageDirectory()
                        + session.getUri());
            } catch (Exception e) {
                e.printStackTrace();
            }
            r = new Response(Response.Status.OK, "audio/mpeg", fis);
        } else {
            String msg = "<html><body><h1>Hello</h1>\n";
            msg += "<p> we serve v3" + session.getUri() + "" + Environment.getExternalStorageDirectory() + session.getUri() + " !</p></html></body>";
            r = new Response(msg);
        }
        return r;
    }

    private String jsonBuilder(FichierMusique fm) {
        JSONObject js = new JSONObject();

        try {
            js.put("album", fm.getAlbum());
            js.put("artist", fm.getArtist());
            js.put("composer", fm.getComposer());
            js.put("genre", fm.getGenre());
            js.put("id", fm.getId());
            js.put("path", fm.getServerPath());
            js.put("title", fm.getTitle());
            js.put("duration", fm.getDuration());

        } catch (Exception e) {

        }
        return js.toString().replace("\\","");

    }
}
