package com.example.raphael.server;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    Nano server;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        try {
            server = new Nano(8080);
            server.start();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
